<?php
/**
 * Template Name: Valvulas
 */
?>
<?php get_header() ?>
  <section class="section-accesorios">
    <div class="margin background-section">
      <div class="section-accesorios-texto">
          <div class="titulo-accsesorios"><?php the_title() ?></div>
        <div class="subtitulo-accsesorios line-titulo">APOLLO</div>
      </div>

      <div>
        <a class="lk" id="menu-responsive-accesorios" href="#">
          <div class="menu-responsive">
            Nuestras marcas
          </div>
        </a>
          <div class='filtro filtro-menu' id="tabs">
            <div id='mostrar-menu' class="filtro-marcas">
              <ul class="ul-marcas">
                <li><a class="btn-6" href="#tabs-1">Alco</a></li>
                <li><a class="btn-6" href="#tabs-2">Apollo</a></li>
                <li><a class="btn-6" href="#tabs-3">Circo</a></li>
                <li><a class="btn-6" href="#tabs-4">Hi-lok</a></li>
                <li><a class="btn-6" href="#tabs-5">IFC</a></li>
                <li><a class="btn-6" href="#tabs-6">KF Valves</a></li>
                <li><a class="btn-6" href="#tabs-7">SHURJOINT</a></li>
              </ul>
            </div>

            <div class="section-recuadro-accesorios" id="tabs-1">
              <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_19series.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_29Series.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_102S.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_120T.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_15-Series.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_32-100.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_32-200.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_72-100.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_76-100.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_76-500.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_76f.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_121T-LF.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HRP_500_series.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/indicador-de-nivel-bypass-9063-2694671.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/imagesCAIMX718.jpg" alt=""></div></a>
            </div>
            <div class="section-recuadro-accesorios" id="tabs-2">

            </div>
            <div class="section-recuadro-accesorios" id="tabs-3">
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/hydroseal25series.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-1.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-3.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-4.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-5.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-7.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MOD_5450.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3208-H.png" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3900.png" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/hydroseal25series-1.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELLO-3100-E.png" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-40MU.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3100P-3100P1.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3200.png" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3208-D.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3208-V.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3300-CONTROLADOR-NEUMATICO-DE-PRESION.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3350-3360-CONTROLADOR-DE-PRESION.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-3520-MEDIDOR-DE-NIVEL-DE-LIQUIDO.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5100-VALVULA-DE-DESCARGA.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5127-5126-VALVULA-DE-DESCARGA.png" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5300-VALVULA-DE-CONTROL-DE-3-VIAS.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5400.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5500-VALVULA-DE-CONTROL.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5600-REGULADOR-DE-PRESION.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5660-REGULADOR-DE-ALTA-PRESION-MODELO-5670-ACERO-INOXIDABLE.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MODELO-5800-VALVULA-DE-RETENCION-TIPO-PISTON.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-1-1.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-5-1.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-7-1.jpg" alt=""></div></a>
                <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Sin-titulo-2.jpg" alt=""></div></a>
            </div>
            <div class="section-recuadro-accesorios" id="tabs-4">
              <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tubing-1.jpg" alt=""></div></a>
          </div>
          <div class="section-recuadro-accesorios" id="tabs-5">
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/homepage.png" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/ismix-250x250.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/castsimplex.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/fabsimplex-1.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/faby.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/zz.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/zzz.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/zzzz.jpg" alt=""></div></a>
        </div>
        <div class="section-recuadro-accesorios" id="tabs-6">
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/VALVULA-14.png" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/1-1.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/3-1.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/4-1.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/5-1.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/6.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/7.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/8.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Ball-Check-Series-60.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Threaded-Series-31.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Wafer-Style-Series-10s.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/SERIE-WB-CUERPO-SOLDADO.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/VALVULA-10.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/VALVULA-11.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/VALVULA-12.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/VALVULA-15.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/VALVULA-17.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Flanged-Series-35.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Wafer-Style-Series-10.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Wafer-Style-Series-12.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Wafer-Style-Series-18.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Check-Valves-Wafer-Style-Series-20-22.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Needle-Valves-Styles-O-Ring.jpg" alt=""></div></a>
          <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/Needle-Valves-Styles-Packed.jpg" alt=""></div></a>

      </div>

    </div>
    </section>



  <!-- <div class="alto-valvulas">

  </div> -->
  <!-- modal -->
  <!-- <div class="modal-background" id='modal'>
    <div class="modal-content">
      <div class="cerrar">
        <a class="close" href="#"></a>
      </div>
      <div class="modal-img">
        <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-CUBE-STREET-LIGHT.jpg" width="200" height="200" alt=""></div>
      <div class="modal-texto">
        <h2 class="titulo-modal">Titulo</h2>
        <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic laudantium ipsum expedita minus perspiciatis, quod quos iusto aliquid recusandae, dolorum <o></o>dit vero suscipit dicta minima officiis. Minus ipsam accusamus consequuntur!</p>
        <div class="btn-7">CONTACTENOS</div>

      </div>
    </div>
  </div> -->
<!-- /modal -->
  <!-- <div class="modal-background" id='modal'>
    <div class="modal-content">
      <div class="cerrar">
        <a class="close" href="#"></a>
      </div>
      <div class="modal-img">
        <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-ECO-GIMBAL.jpg" width="200" height="200" alt=""></div>
      <div class="modal-texto">
        <h2>Titulo</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic laudantium ipsum expedita minus perspiciatis, quod quos iusto aliquid recusandae, dolorum <o></o>dit vero suscipit dicta minima officiis. Minus ipsam accusamus consequuntur!</p>
        <div class="btn-4">CONTACTENOS</div>

      </div>
    </div>
  </div> -->



  <?php include('slidercliente.php') ?>
  <?php include('call.php') ?>
  <?php include('formulario.php') ?>
  <?php include ('testimonios.php'); ?>
  <?php get_footer() ?>
