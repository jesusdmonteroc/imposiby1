<?php
/**
 * Template Name: Accesorios
 */
?>
<?php get_header() ?>
  <section class="section-accesorios">
    <div class="margin background-section">
      <div class="section-accesorios-texto">
          <div class="titulo-accsesorios">ACCESORIOS</div>
        <div class="subtitulo-accsesorios line-titulo">PARA TUBERIA</div>
      </div>

      <div>
        <a class="lk" id="menu-responsive-accesorios" href="#">

            <div class="section-recuadro-accesorios" id="tabs-1">
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/acc7.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/acc11.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/flanche-2.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/socolet.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/WMB2013.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/descarga.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/images.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/images1.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/images2.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/images3.jpg" alt=""></div></a>
              <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/images4.jpg" alt=""></div></a>

            </div>

    </div>
  </section>

  <!-- <div class="modal-background" id='modal'>
    <div class="modal-content">
      <div class="cerrar">
        <a class="close" href="#"></a>
      </div>
      <div class="modal-img">
        <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-CUBE-STREET-LIGHT.jpg" width="200" height="200" alt=""></div>
      <div class="modal-texto">
        <h2 class="titulo-modal">Titulo</h2>
        <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic laudantium ipsum expedita minus perspiciatis, quod quos iusto aliquid recusandae, dolorum <o></o>dit vero suscipit dicta minima officiis. Minus ipsam accusamus consequuntur!</p>
        <div class="btn-7">CONTACTENOS</div>

      </div>
    </div>
  </div> -->


  <!-- <div class="alto-accesorios">

  </div> -->

  <?php include ('slidercliente.php') ?>
  <?php include('call.php') ?>
  <?php include('formulario.php') ?>
  <?php include ('testimonios.php'); ?>
  <?php get_footer() ?>
