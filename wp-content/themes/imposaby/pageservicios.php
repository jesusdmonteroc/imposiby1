<?php
/**
 * Template Name: Serivios
 */
  ?>
  <?php get_header() ?>

<section class="section-nuestros-servicios-page">

  <div class="titulo-page">
    <div class="">
      <h1 class="titulo"><?php the_title() ?></h1>
    <div class="line-titulo">Imposabys</div>
  </div>
  </div>

<section class="section-nuestros-servicios-content">

  <article class="pagina-servicio margin">
    <div class='pagina-servicio-texto'>
      <h2 class="titulo">COMPRAS Y DESPACHOS INTERNACIONALES
        <div class="linea1"></div>
      </h2>
      <p class="parrafo">En Sabys Garcia , a través de alianzas estratégicas y representaciones con empresas y fabricantes de USA y EUROPA, prestamos los servicios para entrega de materiales:
        EX-WORK
        FOB
        FCA
        DDP
        ZONA FRANCA CARTAGENA
        Pueden contar con nosotros como  su agente de compras internacionales  ,  consecución de materiales, repuestos y equipos que comúnmente son adquiridos bajo necesidades puntuales por parte del área de ingeniería y proyectos. Somos su garantía en Colombia, respaldamos y financiamos su compra ahorrándoles tiempo y dinero.</p>
        <div class="btn-5">Contáctenos</div>
    </div>
    <div>
      <img class="img-servicios" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/4187757_m.jpg" width='400' alt="">
    </div class="container-news">
  </article>

  <div class="container-background">
  <article class="pagina-servicio-right margin">
    <div class='pagina-servicio-texto'>
      <h2 class="titulo">SEMINARIOS Y CAPACITACIONES
        <div class="linea2"></div>
      </h2>
        <p class="parrafo">Imposabys, a través de nuestro equipo técnico, especialistas de producto y asociados, ofrecemos a sus departamentos de ingeniería y proyecto en instrumentación y automatización, seminarios y capacitaciones a fin de actualizarlos e instruirlos de las nuevas tecnologías que están dentro de nuestro portafolio de productos. Contáctenos y envíenos su información completa vía email a  HYPERLINK "mailto:info@imposabys.com" info@imposabys.com Estamos enviando boletines a su correo electrónico a fin de que participe en los distintos seminarios que se realizan en cualquier ciudad del país, en el lugar que asi usted lo requiera. Sin costo alguno,
        </p>
        <div class="btn-5">Contáctenos</div>
    </div>
    <div>
      <img class="img-servicios" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/4187757_m.jpg" width='400' alt="">
    </div>
  </article>
</div>

  <article class="pagina-servicio margin">
    <div class='pagina-servicio-texto'>
      <h2 class="titulo">CATALOGOS Y MATERIAL DE SOPORTE
      <div class="linea1"></div></h2>
      <p class="parrafo">Para solicitar material de soporte tales como: Catálogos, Presentaciones, favor enviar vía email a  <a href="mailto:info@imposabys.co">info@imposabys.com</a>  Su información completa y dirección de correo electrónico.
      </p>
        <div class="btn-5">Contáctenos</div>
    </div>
    <div>
      <img class="img-servicios" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/4187757_m.jpg" width='400' alt="">
    </div>
  </article>

  <div class="container-background">
  <article class="pagina-servicio-right margin">
    <div class='pagina-servicio-texto'>
      <h2 class="titulo">ENTREGAS ZONA FRANCA CARTAGENA
      <div class="linea2"></div>
    </h2>
      <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia unde harum officia voluptatem voluptate. Eius dolores quis, repellat sunt. Nihil omnis esse saepe expedita sequi optio, temporibus praesentium alias incidunt?</p>
        <div class="btn-5">Contáctenos</div>
    </div>
    <div>
      <img class="img-servicios" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/4187757_m.jpg" width='400' alt="">
    </div>
  </article>
</div>

</section>

</section>
<?php include('slidercliente.php') ?>
<?php include('call.php') ?>
<?php include ('formulario.php'); ?>
<?php get_footer() ?>
