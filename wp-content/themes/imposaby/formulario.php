<?php
    if(isset($_POST['boton'])){
        if($_POST['nombre'] == ''){
            $errors[1] = '<span class="error">Ingrese su nombre</span>';
        }else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
            $errors[2] = '<span class="error">Ingrese un email correcto</span>';
        }else if($_POST['asunto'] == ''){
            $errors[3] = '<span class="error">Ingrese un asunto</span>';
        }else if($_POST['mensaje'] == ''){
            $errors[4] = '<span class="error">Ingrese un mensaje</span>';
        }else{
            $dest = "tu@email.com"; //Email de destino
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $asunto = $_POST['asunto']; //Asunto
            $cuerpo = $_POST['mensaje']; //Cuerpo del mensaje
            //Cabeceras del correo
            $headers = "From: $nombre <$email>\r\n"; //Quien envia?
            $headers .= "X-Mailer: PHP5\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; //

            if(mail($dest,$asunto,$cuerpo,$headers)){
                $result = '<div class="result_ok">Email enviado correctamente </div>';
                // si el envio fue exitoso reseteamos lo que el usuario escribio:
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['asunto'] = '';
                $_POST['mensaje'] = '';
            }else{
                $result = '<div class="result_fail">Hubo un error al enviar el mensaje </div>';
            }
        }
    }
?>

  <div class="formulario-index margin">
    <div class="titulo-nuestros-servicios">CONTÁCTENOS</div>
    <div class="section-nuestros-servicios-text">
      <div class="line subtitulo">Escribenos</div>
    </div>
    <div class="section-iluminarias-parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
  </div>

  <form class='margin' method='POST' action=''>
    <div class="input-diagramado">

      <input type='text' class='input nombre' name='nombre' placeholder="Nombre completo" value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'><?php if(isset($errors)){ echo $errors[1]; } ?>
      <input type='text' class='input email' name='email' placeholder="Numero de contacto" value='<?php if(isset($_POST['email'])){ $_POST['email']; } ?>'><?php if(isset($errors)){ echo $errors[2]; } ?>
      <input type='text' class='input asunto' name='asunto' placeholder="Correo electronico" value='<?php if(isset($_POST['asunto'])){ $_POST['asunto']; } ?>'><?php if(isset($errors)){ echo $errors[3]; } ?>
      <textarea rows='6' class='input-mensaje mensaje' name='mensaje' placeholder="Mensaje"><?php if(isset($_POST['mensaje'])){ $_POST['mensaje']; } ?></textarea><?php if(isset($errors)){ echo $errors[4]; } ?>
      <div class="btn"><input type='submit' value='Enviar mensaje' class='btn-1 boton' name='boton'></div>
      <?php if(isset($result)) { echo $result; } ?>
      <?php if(isset($result)) { echo $result; } ?>
    </div>
  </form>
