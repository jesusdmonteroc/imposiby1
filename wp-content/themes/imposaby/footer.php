<footer class="footer">
  <div class="footer-diagramado margin">
    <div class="footer-col-1 parrafo-1">
      <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/logo-footer.png" width="120" alt="">
      <p class='parrafo-1'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <p class='parrafo-1'>Calle 33 # 17-51 Barrio Teusaquillo <br>
      Cel: (+57) 300 655 8078 - (+57) 300 686 6643 <br>
      Telefono: (57) 1 340 48 33 <br>
      Email: vgarcia@imposabys.com <br>
      Email: mgarcia@imposabys.com</p>
    </div>
    <div class="footer-col-2">
      <div class="">
        <strong>Visita</strong>
      </div>
      <ul class="lk">
        <a class="lk link" href="http://proyectos.suwwweb.com/imposabys"><li>Inicio</li></a>
        <a class="lk link" href="http://proyectos.suwwweb.com/imposabys/quienes-somos-2"><li>¿Quienes somos?</li></a>
        <a class="lk link" href="http://proyectos.suwwweb.com/imposabys/eventos"><li>Eventos</li></a>
        <a class="lk link" href="http://proyectos.suwwweb.com/imposabys/clientes"><li>Clientes</li></a>
        <a class="lk link" href="http://proyectos.suwwweb.com/imposabys/servicios"><li>Servicios</li></a>
      </ul>
    </div>
    <div class="footer-col-3">
      <div>
        <strong>Siguenos</strong>
      </div>
      <ul class="lk">
        <a class="lk link" href="#"><li class="facebook">Facebook</li></a>
        <a class="lk link" href="#"><li class="twitter">Twitter</li></a>
        <a class="lk link" href="#"><li class="instagram">Instagram</li></a>
        <a class="lk link" href="#"><li class="google">Google +</li></a>
        <a class="lk link" href="#"><li class="youtube">Youtube</li></a>
      </ul>
    </div>
    <div class="footer-col-4">
      <div>
        <strong>Categorias</strong>
      </div>
      <ul class="lk tag-orden">
        <a class="tag" href="http://proyectos.suwwweb.com/imposabys/luminarias"><li>Luminarias</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/valvulas"><li>Valvulas</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/accesorios"><li>Accesorios</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/servicios"><li>servicios</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/imposabys/tuberias"><li>Tuberias</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/imposabys/quienes-somos-2"><li>Mision</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/imposabys/quienes-somos-2"><li>Vision</li></a>
        <a class="tag" href="http://proyectos.suwwweb.com/luminarias"><li>Philips</li></a>
      </ul>
    </div>
  </div>

</footer>
<div class='header-padre'>
  <div class="margin creditos">
    Hecha por SuWWWeb - Páginas web Bogota
  </div>
</div>
<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js' type="text/javascript"></script> -->
<?php wp_footer(); ?>
</body>
</html>
