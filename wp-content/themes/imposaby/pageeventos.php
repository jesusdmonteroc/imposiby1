<?php
/**
 * Template Name: Eventos
 */
  ?>
  <?php get_header() ?>

<section class="section-nuestros-servicios-page">

  <div class="titulo-page">
    <div class="margin">
      <h1><?php the_title() ?></h1>
    <div class="line-titulo">Imposabys</div>
  </div>
  </div>
<div class="container-slider-evento">
  <?php echo do_shortcode('[rev_slider alias="eventos"]') ?>
</div>


</section>
<?php include ('slidercliente.php') ?>
<?php include('call.php') ?>
<?php include ('testimonios.php'); ?>
<?php get_footer() ?>
