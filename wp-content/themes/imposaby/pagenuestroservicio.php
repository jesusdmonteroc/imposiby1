<?php
/**
 * Template Name: Nuestros Servicios
 */
  ?>
  <?php get_header() ?>
<link rel="stylesheet" href="index.css">

<section>

  <div class="titulo-page">
    <div class="margin">
      <h1><?php the_title() ?></h1>
    <div class="line-titulo">Imposabys</div>
  </div>
  </div>

</section>
<?php include('slidercliente.php') ?>
<?php include('call.php') ?>
<?php include('mapa.php') ?>
<?php include ('testimonios.php'); ?>
<?php get_footer() ?>
