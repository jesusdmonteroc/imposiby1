<?php get_header() ?>
  <!-- slider -->
  </section>
  <!-- /slider -->

  <!-- luminarias -->
  <section class="section-iluminarias margin">
    <div class='section-iluminarias-diagramado'>
      <div>
        <h2 class='section-iluminarias-titulo'>LUMINARIAS</h2>
        <p class='section-iluminarias-subtitulo'>Nuestros productos</p>
        <p class='section-iluminarias-parrafo'>Selecciona los productos a visualizar</p>
      </div>
      <div class='filtro'>
        <li><a class="btn-3" href="">ILUMINARIAS</a></li>
        <li><a class="btn-3" href="">VALVULAS</a></li>
        <li><a class="btn-3" href="">TUBERIAS</a></li>
      </div>
    </div>
    <div class="section-iluminarias-productos">
      <div class='section-iluminarias-productos-detalle'>
        <img src="img/GW CUBE STREET LIGHT.jpg" width='210' alt="">
      </div>
      <div class='section-iluminarias-productos-detalle'>
        <img src="" alt="">
      </div>
      <div class='section-iluminarias-productos-detalle'>
        <img src="" alt="">
      </div>
      <div class='section-iluminarias-productos-detalle'>
        <img src="" alt="">
      </div>
      <div class='section-iluminarias-productos-detalle'>
        <img src="" alt="">
      </div>
    </div>
    <div class="btn">
      <a class='btn-1' href="">
        VER ILUMINARIAS
      </a>
    </div>
  </section>
  <!-- /luminarias -->
  <!-- acerca de nosotros -->
  <section class='section-quiere-saber-mas'>
    <div class="section-quiere-saber-mas-diagramado margin">
      <h2 class="titulo">
        ¿QUIERES SABER MÁS ACERCA DE NOSOTROS?
      </h2>
      <div class="linea"></div>
      <P class='subtitulo'>Sabys Garcia  Comenzó su actividad en el año 1992 como proveedor de la industria pesada colombiana, enfocado especialmente en la industria de producción, distribución y refinación de hidrocarburos.</P>
    </div>
    <div class="btn">
      <a class="btn-2 mas" href="">VER MÀS &ensp;</a>
    </div>
  </section>
  <!-- /acerca de nosotros -->
  <!-- nuestros servicios -->
  <section class="section-nuestros-servicios">
    <div class="margin section-nuestros-servicios-diagramado">
      <div class="titulo-nuestros-servicios">NUESTROS SERVICIOS</div>
      <div class="section-nuestros-servicios-text">
        <div class='line subtitulo'>Lo que hacemos</div>
      </div>
      <div class="section-iluminarias-parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro excepturi,</div>
      <div class="section-nuestros-servicios-items">
        <div class='items'>
          <img src="img/enviado.png" alt="">
          <div class='titulo-items'>COMPRAS Y DESPACHOS <br>INTERNACIONALES</div>
          <div class="parrafo">En Sabys Garcia , a través de alianzas estratégicas y representaciones con empresas y fabricantes de USA y EUROPA  prestamos los servicios para entrega de materiales.</div>
        </div>
        <div class='items'>
          <img src="img/presentacion.png" alt="">
          <div class='titulo-items'>SEMINARIOS Y <br>CAPACITACIONES</div>
          <div class="parrafo">Imposabys, a través de nuestro equipo técnico, especialistas de producto y asociados, ofrecemos a sus departamentos de ingeniería y proyecto en instrumentación y automatización</div>
        </div>
        <div class='items'>
          <img src="img/panfleto-grande.png" alt="">
          <div class='titulo-items'>CATALOGOS Y <br>MATERIAL DE SOPORTE</div>
          <div class="parrafo">Para solicitar material de soporte tales como: Catálogos, Presentaciones, favor enviar vía email a info@imposabys.com</div>
        </div>
        <div class='items'>
          <img src="img/paquetes-de-entrega-en-un-carro.png" alt="">
          <div class='titulo-items'>ENTREGAS <br>ZONA FRANCA CARTAGENA</div>
          <div class="parrafo">Pueden contar con nosotros como  su agente de compras internacionales, consecución de materiales, repuestos y equipos que comúnmente son adquiridos bajo necesidades puntuales .</div>
        </div>
      </div>
      <div class="btn">
        <div class="btn-1 mas-1">
          VER SERVICIOS &ensp;
        </div>
      </div>
    </div>
  </section>
  <!-- /nuestros servicios -->
  <!-- calltoaction -->
  <?php include ('call.php'); ?>
  <!-- /calltoaction -->
  <!-- nuestros clientes -->
  <section class="section-nuestros-servicios">
    <div class="margin section-nuestros-servicios-diagramado">
      <div class="titulo-nuestros-servicios">NUESTROS CLIENTES</div>
      <div class="section-nuestros-servicios-text">
        <div class="line subtitulo">Los que confian</div>
      </div>
      <div class="section-iluminarias-parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro excepturi, facere atque autem! Ad deleniti ex itaque, aut fuga. Quos deserunt ducimus nobis, maxime doloribus eum reprehenderit iusto possimus unde.</div>
      <div>
        slider
      </div>
  </section>
  <!-- /nuestros clientes -->
  <!-- testiomonios -->
  <?php include ('testimonios.php'); ?>

  <!-- /testiomonios -->

<?php include ('formulario.php'); ?>

<?php get_footer() ?>
