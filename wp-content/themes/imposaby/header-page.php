<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <link rel="stylesheet" href="<?php bloginfo(stylesheet_url) ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <title>Imposaby</title>
</head>
<body>
  <!-- header -->
  <div class='header-padre'>
    <div class="margin-header header-sub">
      <div class='header-contacto'>
        <li>Calle 33 # 17-51 Barrio Teusaquillo, Bogotá DC</li>
        <li>(+57) 300 655 8078 — (+57)300 686 6643</li>
        <li>vgarcia@imposabys.com</li>
      </div>
      <div class="header-redes">
        <li><a class="facebook" href=""></a></li>
        <li><a class="twitter" href=""></a></li>
      </div>
    </div>
  </div>
  <!-- /header -->


  <header  class='header'>
    <!-- menu -->
    <nav class="nav-page">
      <div class="margin">
        <ul class='nav-ul'>
          <?php
           wp_nav_menu(
             array(
               'menu-1' => 'menu-izq',
               'menu_class' => ''
             ) )
          ?>
          <div>
            <img class="logo" src="http://localhost:8888/SuWWWeb/imposaby/web/imposaby/wp-content/uploads/2017/08/logo.png" alt="">
          </div>
          <?php
           wp_nav_menu(
             array(
               'menu-2' => 'menu-der',
               'menu_class' => ''
             ) )
          ?>
        </ul>
        <div class="telefono comunicate">
          ¡Comunícate!<br>
          (+57) 300 655 8078
        </div>
      </div>
    </nav>

    <!-- <nav class="mr" id='mr'>
      <div class="">
        <ul class=''>
            <img src="img/logo.png" width='150' alt="">
          </div>
          <?php
           wp_nav_menu(
             array(
               'theme_location' => 'header',
               'menu_class' => '',
             ) )
          ?>
        </ul>
      </div>
    </nav> -->

    <!-- /menu -->
  </header>
