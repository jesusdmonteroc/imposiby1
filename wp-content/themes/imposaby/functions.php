<?php
add_theme_support( 'post-thumbnails' );

// function register_my_menu(){
//   register_nav_menu(
//     array(
//       'menu-izq' => __('Este es menu'),
//       'menu-der' => __('Este es otro menu')
//     )
//   );
// }


register_nav_menus(
array(
'menu-izquierdo' => 'Menu izquierdo', // main nav in header
'menu-derecho' => 'Menu derecho', // secondary nav in footer
'menu-accesorios' => 'Menu accesorios' // secondary nav in footer
)
);


function mi_inicio() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_enqueue_script( 'uij','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', true);
		wp_enqueue_script( 'jjj','https://code.jquery.com/ui/1.12.1/jquery-ui.js', true);
    wp_enqueue_script( 'js', get_template_directory_uri() . '/js/index.js',false,'1.1','all');
		wp_enqueue_script( 'validacion', get_template_directory_uri() . '/js/validacion.js',false,'1.1','all');
	}
}
add_action('init', 'mi_inicio');
