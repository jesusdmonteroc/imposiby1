<?php get_header() ?>
  <!-- slider -->
  <?php echo do_shortcode('[rev_slider alias="index"]' ) ?>

  </section>
  <!-- /slider -->
  <!-- luminarias -->
  <section class="section-iluminarias-index">
    <div class='section-iluminarias-diagramado margin'>

      <div class='filtro-section-index'>
        <div class='filtro' id="tabs">

          <ul class="filtro-index ul">
            <li><a class="btn-3 active" href="#tabs-1">ILUMINARIAS</a></li>
            <li><a class="btn-3" href="#tabs-2">VÁLVULAS</a></li>
            <li><a class="btn-3" href="#tabs-3">TUBERIAS</a></li>
          </ul>
          <div class="section-recuadro-index" id="tabs-1">
            <div class="titulos-container">
              <h2 class='section-iluminarias-titulo'>LUMINARIAS</h2>
              <p class='section-iluminarias-subtitulo line-after'>Nuestros productos</p>
              <p class='section-iluminarias-parrafo'>Selecciona los productos a visualizar</p>
            </div>
            <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-CUBE-STREET-LIGHT.jpg"></div></a>
            <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-LED-FLOOD-LIGHT.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/leds-gu10.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/PANEL-LED-REDONDO.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-5W.jpg" alt=""></div></a>
            <div class="btn-container">
              <a class='btn-1' href="http://proyectos.suwwweb.com/imposabys/luminarias/">
                VER ILUMINARIAS
              </a>
            </div>
          </div>
          <div class="section-recuadro-index" id="tabs-2">
            <div class="titulos-container">
              <h2 class='section-iluminarias-titulo'>VÁLVULAS</h2>
              <p class='section-iluminarias-subtitulo line-after'>Nuestros productos</p>
              <p class='section-iluminarias-parrafo'>Selecciona los productos a visualizar</p>
            </div>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/fabsimplex.jpg"></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/2.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/3.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/4.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="valvulas-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/5.jpg" alt=""></div></a>
            <div class="btn-container">
              <a class='btn-1' href="http://proyectos.suwwweb.com/imposabys/valvulas/">
                VER VÁLVULAS
              </a>
            </div>
          </div>
          <div class="section-recuadro-index" id="tabs-3">
            <div class="titulos-container">
              <h2 class='section-iluminarias-titulo'>TUBERIAS</h2>
              <p class='section-iluminarias-subtitulo line-after'>Nuestros productos</p>
              <p class='section-iluminarias-parrafo'>Selecciona los productos a visualizar</p>
            </div>
            <a id='modal-accesorios' href="#"><div class="tuberias-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tuberia1.gif"></div></a>
            <a id='modal-accesorios' href="#"><div class="tuberias-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tuberia2.png" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="tuberias-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tuberia3.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="tuberias-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tuberia4.jpg" alt=""></div></a>
            <a id='modal-accesorios' href="#"><div class="tuberias-item"><img class="item-accesorio" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tuberia5.jpg" alt=""></div></a>
            <div class="btn-container">
              <a class='btn-1' href="http://proyectos.suwwweb.com/imposabys/tuberia/">
                VER TUBERIA
              </a>
            </div>
          </div>
        </div>
      </div>
      </div>
  </section>

  <!-- /luminarias -->
  <!-- acerca de nosotros -->
  <section class='section-quiere-saber-mas'>
    <div class="section-quiere-saber-mas-diagramado margin">
      <h2 class="titulo">
        ¿QUIERES SABER MÁS ACERCA DE NOSOTROS?
      </h2>
      <div class="linea"></div>
      <P class='subtitulo'>Sabys Garcia  Comenzó su actividad en el año 1992 como proveedor de la industria pesada colombiana, enfocado especialmente en la industria de producción, distribución y refinación de hidrocarburos.</P>
    </div>
    <div class="btn">
      <a class="btn-2 mas" href="http://proyectos.suwwweb.com/imposabys/quienes-somos-2/">VER MÀS &ensp;</a>
    </div>
  </section>
  <!-- /acerca de nosotros -->
  <!-- nuestros servicios -->
  <section class="section-nuestros-servicios">
    <div class="margin section-nuestros-servicios-diagramado">
      <div class="titulo-nuestros-servicios">NUESTROS SERVICIOS</div>
      <div class="section-nuestros-servicios-text">
        <div class='line subtitulo'>Lo que hacemos</div>
      </div>
      <div class="section-iluminarias-parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro excepturi,</div>
      <div class="section-nuestros-servicios-items">
        <div class='items'>
          <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/envio.png" alt="">
          <div class='titulo-items'>COMPRAS Y DESPACHOS <br>INTERNACIONALES</div>
          <div class="parrafo">En Sabys Garcia , a través de alianzas estratégicas y representaciones con empresas y fabricantes de USA y EUROPA  prestamos los servicios para entrega de materiales.</div>
        </div>
        <div class='items'>
          <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/clase.png" alt="">
          <div class='titulo-items'>SEMINARIOS Y <br>CAPACITACIONES</div>
          <div class="parrafo">Imposabys, a través de nuestro equipo técnico, especialistas de producto y asociados, ofrecemos a sus departamentos de ingeniería y proyecto en instrumentación y automatización</div>
        </div>
        <div class='items'>
          <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/panfleto.png" alt="">
          <div class='titulo-items'>CATALOGOS Y <br>MATERIAL DE SOPORTE</div>
          <div class="parrafo">Para solicitar material de soporte tales como: Catálogos, Presentaciones, favor enviar vía email a info@imposabys.com</div>
        </div>
        <div class='items'>
          <img src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/caja.png" alt="">
          <div class='titulo-items'>ENTREGAS <br>ZONA FRANCA CARTAGENA</div>
          <div class="parrafo">Pueden contar con nosotros como  su agente de compras internacionales, consecución de materiales, repuestos y equipos que comúnmente son adquiridos bajo necesidades puntuales .</div>
        </div>
      </div>
      <div class="btn">
        <a class="lk" href="http://proyectos.suwwweb.com/imposabys/servicios/">
          <div class="btn-1 mas-1">
          VER SERVICIOS &ensp;
        </div>
        </a>
      </div>
    </div>
  </section>
  <!-- /nuestros servicios -->
  <!-- calltoaction -->
  <?php include ('call.php'); ?>
  <!-- /calltoaction -->
  <!-- nuestros clientes -->
  <?php include ('slidercliente.php') ?>

  <!-- /nuestros clientes -->
  <!-- testiomonios -->
  <?php include ('testimonios.php'); ?>

  <!-- /testiomonios -->

<?php include ('formulario.php'); ?>

<?php get_footer() ?>
