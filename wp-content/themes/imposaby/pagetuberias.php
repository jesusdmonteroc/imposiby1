<?php
/**
 * Template Name: Tuberias
 */
  ?>
  <?php get_header() ?>
<link rel="stylesheet" href="index.css">

<section class="section-nuestros-servicios-page">
  <div class="titulo-page">
    <div class="margin">
      <h1 class="titulo"><?php the_title() ?></h1>
    <div class="line-titulo subtitulo">Imposabys</div>
  </div>
  </div>

  <div class="section-tuberia margin">
    <div class="parrafo-tuberia">
      <h1 class="titulo">TUBERÍAS DE LINEA DE PRODUCCIÒN Y PROCESO EN ACERO CARBÓN, INOXIDABLE Y ALEACIONES ESPECIALES</h1>
      <div class="linea1">
      </div>
      <p class="parrafo">
        En acero inoxidable,cobre, bronce, acero y aleaciones especiales: Electro soldadas (ERW) y sin soldadura. Espesores desde 0,035'' WT en adelante y diámetros desde 1/8" OD a 2" OD.Medidas milimétricas, calidades opaco y brillante para acero inoxidable, presentación tipo: Rígido y flexible en largos de 20 pies, también en rollos. Normas ASTM A269, DIN. Tubing para calderas (Boiler), condensadores e intercambiadores de calor, Tubing en acero ( Mecánico): Cold Drawn Butt Weld. Largos sobre necesidad puntual. Normas ASTM,DIN.
        TUBERÍAS DE LINEA, PRODUCCIÓN Y PROCESO EN ACERO CARBÓN, INOXIDABLE Y ALEACIONES ESPECIALES

        <li class="parrafo">Tubería en Acero Carbono con y sin Costura:  ASTMA106 Gr B  API5L Gr hasta API5L Grx65</li>
        <li class="parrafo">Tubería en Acero inoxidable: ASTM A312 TIP 304,TIP 316,TIP 317, TIP 321</li>
        <li class="parrafo">Tubería para Intercambiadores</li>
        <li class="parrafo">Tuberías y Accesorios aleados</li>
      </p>
    </div>
    <div class="slider-tuberia">
      <?php echo do_shortcode( '[rev_slider alias="tuberias"]' ) ?>
    </div>
  </div>
  </div>

</section>
<?php include('slidercliente.php') ?>
<?php include('call.php') ?>
<?php include ('formulario.php'); ?>
<?php get_footer() ?>
