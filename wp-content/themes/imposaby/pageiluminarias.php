<?php
/**
 * Template Name: Luminarias
 */
 ?>
 <?php get_header() ?>
   <section class="section-iluminarias">
     <div class="margin background-section">
       <div class="section-accesorios-texto">
           <div class="titulo-accsesorios"><?php the_title() ?></div>
         <div class="subtitulo-accsesorios line-titulo">GOOD WORK</div>
       </div>

       <div>
         <a class="lk" id="menu-responsive-accesorios" href="#">
           <div class="menu-responsive">
             Nuestras marcas
           </div>
         </a>
           <div class='filtro filtro-menu' id="tabs">
             <div id='mostrar-menu' class="filtro-marcas">
               <div class="background-items">
               <ul class="ul-marcas">
                 <li><a class="btn-6" href="#tabs-1">Philips</a></li>
                 <li><a class="btn-6" href="#tabs-2">Ilumax</a></li>
                 <li><a class="btn-6" href="#tabs-3">Silvanya</a></li>
                 <li><a class="btn-6" href="#tabs-4">Goodwork</a></li>
                 <li><a class="btn-6" href="#tabs-5">Tecno lite</a></li>
               </ul>
               </div>
             </div>
             <div class="section-recuadro-accesorios" id="tabs-1">
              <a id='modal-accesorios' href="#">
                <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/coreline-panel-1.png" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> CoreLine Panel</h2>
                   <p class="parrafo hidden">Initial Performance (IEC Compliant)<br>

Mechanical and Housing<br>
Initial Luminous flux<br>
3400lm<br>
Geometry<br>
W60L60 (Width 0.60 m, length 0.60m)<br>
Initial LED Luminaire<br>
81 lm/W<br>
Housing Material<br>
steel<br>
efficacy<br>

Gear Tray material<br>
Plastic<br>
Init Corr Color<br>



Temperature<br>
4000 K-6500K


Init Color Rendering<br>
>80


Index<br>



Initial chromaticy<br>
(0.38  0.38) SDMC>5<br>

</p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/master-colour-cdm-r.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> Master Colour CDM-R</h2>
                   <p class="parrafo hidden"></p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/essential-tled-ecofit.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> Essential TLED Ecofit</h2>
                   <p class="parrafo hidden"></p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GreenUp-Highbay-BY550.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> GreenUp Highbay-BY550</h2>
                   <p class="parrafo hidden">
                    product spec<br>
                    BY550P<br>
                    BY550P/li><br>
                     System Output<br>
                     10000/13000/20000lm<br>
                     10000/13000/20000lm<br>
                     System Power<br>
                     80/110/160 W<br>
                     80/110/160 W<br>
                     System Efficiency<br>
                     125lm/W<br>
                     125lm/W<br>
                     >Control<br>
                     fixed /DALI<br>
                     wireless<br>
                    Lifetime<br>
                     70000 Hrs<br>
                     70000hrs ( life time extend to 100000 hrs with average 30% dimming down<br>
</p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/LED-HIGH-BAY-FBX.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> LED High Bay FBX</h2>
                   <p class="parrafo hidden">Especificaciones
                     <span>Informacion General</span>
                     Tipo de Bombilla: LED<br>
                     Color de Luz: Blanca neutro y blanca fria<br>
                     Eficacia Luminosa LED: 8.000, 12.000, 16.000, 20.000, 24.000, 36.000 y 45.000 lumenes nominales<br>
                     Temperatura de color: 4000 K/5000k<br>
                     Indice de reproduccion de color: 80<br>
                      Vida Util: 100.000 horas a 40º C<br>
                     60.000 horas a 40º C<br>
                     (para 8Klm y 16 Klm)<br>
 	                  </p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/led-high-bay-hbx.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> Informacion General</h2>
                   <p class="parrafo hidden">
                    Tipo de Bombilla: LED<br>
                    Color de Luz: Blanca neutro<br>
                    Eficacia Luminosa LED: 13Lm/ 13.000  lumenes nominales<br>
                    17 Lm/ 17.000 Lumenes nominales<br>
                    24Lm/ 24.000 Lumenes Nominales<br>
			              </p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/leds-gu10-1.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> Master LED GU10</h2>
                   <p class="parrafo hidden"></p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/master-led-gu10.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> LEDs GU 10</h2>
                   <p class="parrafo hidden"></p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/SMARTLED-HIGHBAY.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> Smart Led Highbay</h2>
                   <p class="parrafo hidden"><strong></strong>
                     Tipo: highbay LED<br>
                     Flujo luminoso: 9000 lm, 14.000 lm,20000 lm y 24.000 lm<br>
                     Consumo luminaria: 90W, 140 W, 200W Y 240W<br>
                     Tc y IRC: Blanco 4000K con IRC 85<br>
                     Vida Util: 50.000 horas @L70</p>
                 </div>
               </a>
               <a id='modal-accesorios' href="#">
                 <div class="accesorio-item">
                   <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/tubo-tl5-essential.jpg" alt="">
                   <h2 class="titulo-modal hidden"><div class="linea1">
                   </div> Tubo TL5 ESSENTIAL</h2>
                   <p class="parrafo hidden"></p>
                 </div>
               </a>
             </div>
             <div class="section-recuadro-accesorios" id="tabs-2">
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/BOMBILLO-LED-30000-HORAS-DE-VIDA-4W-1.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/REFLECTOR-LED-2OW-1.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/REFLECTOR-LED-100W-1.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/REFLECTOR-LED-RECARGABLE-1.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/REFLECTOR-LED-SLIM-1OW-1.jpg" alt=""></div></a>
             </div>
             <div class="section-recuadro-accesorios" id="tabs-3">
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GC018.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GC410.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GC510.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GC616.png" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/HBL.png" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/LED-PANEL-SQ-24W-DL-UNV-35H.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/LED-STREET-LIGHT-60W-NW-SHARK.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/LED-STREET-LIGTH-60W-NW-ZD88.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/PANEL-LED-REDONDO-1.jpg" alt=""></div></a>
               <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/PANEL-LED-SQ-45W-NW-DL-UNV-IVY-1.jpg" alt=""></div></a>
           </div>

           <div class="section-recuadro-accesorios" id="tabs-4">
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-CUBE-STREET-LIGHT-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW LED FLOOD LIGH</h2>
                 <p class="parrafo hidden"><strong>CARACTERISTICAS MECANICAS</strong>
                   Difusor: Transparente<br>
                   Acabado: Aluminio<br>
                   Disipador: Aluminio<br>
                   Proteccion: IP 65<br>
                   Proteccion IK: 08 <br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
                 </p>
               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-ECO-GIMBAL-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW ECO GIMBAL</h2>
                 <p class="parrafo hidden"><strong>CARACTERISTICAS MECANICAS</strong>
                   Difusor: Transparente/ Opal<br>
                   Acabado: Aluminio blanco<br>
                   Disipador: Aluminio<br>
                  Proteccion: IP 20<br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
               </div></a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-FLEX-STRIP-CRI90-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> CARACTERISTICAS ELECTRICAS</h2>
                 <p class="parrafo hidden"><strong></strong>
                   Alimentacion 12/24 DC<br>
                   Frecuencia: Factor de Potencia: 0.90/li<br>
                   Driver externo<br>
                  Regulacion: Controlador<br>
               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-KONIKAL-STREET-LIGHT-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW KONIKAL STREET LIGHT</h2>
                 <p class="parrafo hidden"><strong></strong>
                   Difusor: Transparente<br>
                   Acabado: Negro micro texturado<br>
                   Proteccion: IP 65<br>
                  Proteccion IK 09 Clase I<br>
                  Temperatura de operación: -10º  C/+40º C <br>
                 Vida Util: 50.000 horas<br>

               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-LED-FLOOD-LIGHT-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW LED FLOOD LIGH</h2>
                 <p class="parrafo hidden"><strong>CARACTERISTICAS MECANICAS</strong>
                   Difusor: Transparente<br>
                   Acabado: Aluminio<br>
                   Disipador: Aluminio<br>
                   Proteccion: IP 65<br>
                   Proteccion IK: 08 <br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
                 </p>
               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-LIGHT-BAR-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW LIGHT BAR</h2>
                 <p class="parrafo hidden"><strong>CARACTERISTICAS MECANICAS</strong>
                   Difusor: Transparente<br>
                   Acabado: Aluminio<br>
                   Disipador: Aluminio<br>
                   Proteccion: IP 65<br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-LIRA-STREET-LIGHT-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GE LIRA STREET LIGHT</h2>
                 <p class="parrafo hidden"><strong>Difusor: Opaco/ Transparente</strong>
                   Acabado: Aluminio<br>
                   Disipador: Aluminio<br>
                   Proteccion: IP 65<br>
                   Proteccion IK 10 Clase I<br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-POWER-EYES-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW POWER EYES</h2>
                 <p class="parrafo hidden"><strong>CARACTERISTICAS MECANICAS</strong>
                   Difusor: Transparente<br>
                   Acabado: Aluminio <br>
                   Disipador: Aluminio<br>
                   Proteccion: IP 65br>
                   Proteccion IK 10 Clase I<br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
               </div>
             </a>
             <a id='modal-accesorios' href="#">
               <div class="accesorio-item">
                 <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/GW-RECESS-LITE-1.jpg" alt="">
                 <h2 class="titulo-modal hidden"><div class="linea1">
                 </div> GW RECESS LITE</h2>
                 <p class="parrafo hidden"><strong>CARACTERISTICAS MECANICAS</strong>
                   Difusor: Transparente<br>
                   Acabado: Aluminio <br>
                   Disipador: Aluminio<br>
                   Proteccion: IP 20<br>
                   Temperatura de operación: -10º  C/+40º C <br>
                   Vida Util: 50.000 horas<br>
               </div>
             </a>
         </div>

         <div class="section-recuadro-accesorios" id="tabs-5">
           <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-07-VERDE-1.jpg" alt=""></div></a>
           <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-07W.jpg" alt=""></div></a>
           <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-1W.jpg" alt=""></div></a>
           <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="hhttp://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-45W.jpg" alt=""></div></a>
           <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-5W.jpg" alt=""></div></a>
           <a id='modal-accesorios' href="#"><div class="accesorio-item"><img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED-13W.jpg" alt=""></div></a>
           <a id='modal-accesorios' href="#">
             <div class="accesorio-item">
               <img class="item-accesorio" width="200" height="300" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/MR16-LED.jpg" alt="">
               <h2 class="titulo-modal hidden"><div class="linea1">
               </div> MR16-LED/O,5W	MR16-LED/O,7W LAMPARA DE LED¨S</h2>
               <p class="parrafo hidden"><strong></strong>

             </div>
           </a>
       </div>
     </div>
   </section>

   <div class="modal-background" id='modal'>

   </div>

   <!-- <div class="alto-accesorios">

   </div> -->

   <?php include('slidercliente.php') ?>
   <?php include('call.php') ?>
   <?php include('formulario.php') ?>
   <?php include ('testimonios.php'); ?>
   <?php get_footer() ?>
