<?php
/**
 * Template Name: Quienes somos
 */
  ?>
  <?php get_header() ?>

  <section class="section-nuestros-servicios-page">
   <div class="">
     <div>
         <div class="titulo-page">
           <div class="margin">
             <h1 class="titulo"><?php the_title() ?></h1>
           <div class="line-titulo">Imposabys</div>
         </div>
         </div>
     </div>
     <div class="section-somos margin">
       <div class="parrafo-somos">
         <h1 class="titulo">ACERCA DE NOSOTROS</h1>
         <div class="linea1">

         </div>
         <p class="parrafo">
           Sabys Garcia  Comenzó su actividad en el año 1992 como proveedor de la industria pesada colombiana, enfocado especialmente en la industria de producción, distribución y refinación de hidrocarburos.
 Durante la década de los años 90, la compañía alcanza un gran crecimiento de la mano de los desarrollos petroleros en el centro oriente del país y el magdalena medio y alto, consolidando su servicio con la realización de alianzas estratégicas con fabricantes de clase mundial de válvulas, tuberías y accesorios para tubería.
 Durante la primera década del siglo XXI, continuó su desarrollo ampliando su portafolio de clientes y servicios, enmarcado dentro de una política de mejora continua en busca de trasladar a sus clientes el servicio de suministro de materiales con valores agregados de servicio pre y post-venta.
 Sabys Garcia cuenta con aliados estratégicos en Estados Unidos y a través de su propia compañía le permite brindarle al cliente final un servicio más completo con alternativas de negocios EXWORKS, FCA, FOB Y DDP.
 En la actualidad Sabys Garcia  es representante de fabricantes de clase mundial como Circor Energy, KF Industries, Mallard, Hydroseal, ALCO valves, HYLOK, IFC, Apollo, Shurjoint
         </p>
       </div>
       <div>
         <img class="img-quienes-somos" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/logo.png" alt="">
       </div>
     </div>
   </div>
  </section>
  <?php include('call.php') ?>
  <div class='margin-somos' id="tabs">
    <ul class="container-somos ul">
      <li><a class="btn-3 active" href="#tabs-1">MISIÓN</a></li>
      <li><a class="btn-3" href="#tabs-2">VISIÒN</a></li>
      <li><a class="btn-3" href="#tabs-3">VALORES CORPORATIVOS</a></li>
    </ul>
    <div class="section-recuadro-somos" id="tabs-1">
      <p class="parrafo">Suministrar tuberías, válvulas , accesorios y luminarias  de alta calidad especialmente para el sector de hidrocarburos y generación energética, que satisfagan las necesidades y cumplan con las expectativas de nuestros clientes.
Orientados a ser la mejor opción para nuestros clientes, excediendo sus expectativas y asegurando la calidad de nuestros productos; creciendo y afianzándonos con rentabilidad y sostenibilidad del negocio, en un ambiente altamente competitivo, creando y manteniendo un ambiente de trabajo participativo e innovador, generando oportunidades de crecimiento y desarrollo personal y profesional para nuestros grupo de trabajo y desempeñando nuestras actividades en armonía con el medio ambiente y la Comunidad, logrando la satisfacción de clientes y socios.</p>
    </div>
    <div class="section-recuadro-somos" id="tabs-2">
      <p class="parrafo">Convertirnos en un proveedor modelo para la industria nacional a través del mejoramiento continuo de nuestros procesos, posicionando y consolidando nuestros servicios y marcas representadas; expandiéndonos hacia nuevos mercados latinoamericanos atendiendo operaciones desde nuestra bodega en zona franca.
</p>
    </div>
    <div class="section-recuadro-somos" id="tabs-3">
      <p class="parrafo"><strong>CALIDAD</strong> <br>
Realizamos nuestro trabajo con excelencia.<br>
<strong>RENTABILIDAD</strong> <br>
Generamos utilidades para promover el desarrollo y la prosperidad colectiva.<br>
<strong>RESPONSABILIDAD SOCIAL</strong> <br>
Reconocemos las necesidades y expectativas de nuestros grupos de interés asumiendo consecuentemente el impacto de nuestras acciones, siempre buscando el beneficio común.<br>
<strong>COMPROMISO CON LA SATISFACCIÓN </strong><br>
Compromiso permanente por el mejoramiento continúo de los procesos, para cumplir las expectativas de nuestros clientes. Logrando la satisfacción total de los mismos. <br>
<strong>INTEGRIDAD</strong> <br>
Somos concordantes entre la forma de pensar y actuar, sin importar la situación que se afronte. <br>
<strong>ÉTICA</strong> <br>
Actuamos con justicia, honestidad, rectitud y trasparencia. <br>
<strong>CAMBIO ORIENTADO AL LOGRO</strong> <br>
Enfrentamos proactivamente los retos, nos adaptamos y aprovechamos las actividades del entorno logrando <br>
</p>
    </div>
  </div>
<?php include('slidercliente.php') ?>
<?php include ('testimonios.php'); ?>
<?php get_footer() ?>
