<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" href="img/logo.png" />
   <link rel="stylesheet" href="<?php bloginfo(stylesheet_url) ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Raleway:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <title>Imposabys</title>
  <?php wp_head(); ?>
</head>
<body>
  <!-- header -->
  <div class='header-padre'>
    <div class="margin header-sub">
      <div class='header-contacto'>
        <li class="aqui">Calle 33 # 17-51 Barrio Teusaquillo, Bogotá DC</li>
        <li class="telefono">(+57) 300 655 8078 — (+57)300 686 6643</li>
        <li class='mensaje'>vgarcia@imposabys.scom</li>
      </div>
      <div class="header-redes">
        <li><a class="facebook" href=""></a></li>
        <li><a class="twitter" href=""></a></li>
      </div>
    </div>
  </div>

  <!-- /header -->

  <section class='section-slider'>

  <header  class='header'>
    <!-- menu -->
    <?php if (is_home() || is_front_page()) { ?>
      <nav class="nav">
        <div class="margin-header">
          <ul class='nav-ul'>
            <?php
              wp_nav_menu( array(
                  'theme_location' => 'menu-izquierdo',
                  'container_class' => 'ul' ) );
                        ?>
                        <div>
                          <img class="logo" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/logo.png" alt="">
                        </div>
                        <?php
                          wp_nav_menu( array(
                  'theme_location' => 'menu-derecho',
                  'container_class' => '' ) );
              ?>

          </ul>
          <div class="telefono comunicate">
            ¡Comunícate!<br>
            (+57) 300 655 8078
          </div>
        </div>

      </nav>

    <?php }else{ ?>

      <nav class="nav-page">
        <div class="margin-header">
          <ul class='nav-ul'>
            <?php
              wp_nav_menu( array(
                  'theme_location' => 'menu-izquierdo',
                  'container_class' => 'custom-menu-class' ) );
              ?>
                        <div>
                          <img class="logo" src="http://proyectos.suwwweb.com/imposabys/wp-content/uploads/2017/08/logo.png"  alt="">
                        </div>
                        <?php
                          wp_nav_menu( array(
                  'theme_location' => 'menu-derecho',
                  'container_class' => 'custom-menu-class' ) );
              ?>
          </ul>
          <div class="telefono comunicate">
            ¡Comunícate!<br>
            (+57) 300 655 8078
          </div>
        </div>

      </nav>

    <?php }  ?>

    <!-- <nav class="mr" id='mr'>
      <div class="">
        <ul class=''>
            <img src="img/logo.png" width='150' alt="">
          </div>
          <?php
           wp_nav_menu(
             array(
               'theme_location' => 'header',
               'menu_class' => '',
             ) )
          ?>
        </ul>
      </div>
    </nav> -->

    <!-- /menu -->
  </header>
