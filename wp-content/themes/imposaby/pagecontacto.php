<?php
/**
 * Template Name: Contacto
 */
?>
  <?php get_header() ?>
  <section class="section-nuestros-servicios-page">
    <div class="titulo-page">
      <div class="margin">
        <h1 class="titulo"><?php the_title() ?></h1>
      <div class="line-titulo subtitulo">Imposabys</div>
    </div>
    </div>

  <section class="section-contacto">
    <div class="margin formulario">
      <div class="contacto-col-1">
        <div class="titulo-contacto">
          <strong>CONTÁCTENOS</strong>
          <div class="linea1"></div>
        </div>
        <li class="parrafo-contacto"><strong>OFICINA BOGOTA DC</strong></li>
        <li class="parrafo-contacto aqui">Calle 33 # 17-51 Barrio Teusaquillo</li>
        <li class="parrafo-contacto telefono">Cel: 300 655 8078 - 300 686 6643</li>
        <li class="parrafo-contacto telefono">Telefono: 340 48 33</li>
        <li class="parrafo-contacto mensaje">Email: vgarcia@imposabys.com</li>
        <li class="parrafo-contacto mensaje">Email: vgarcia@imposabys.com</li>
        <br>

        <li class="parrafo-contacto"><strong> OFICINA BARRAQUILLA </strong></li>
        <li class="parrafo-contacto aqui">Carrara 74 # 82-23</li>
        <li class="parrafo-contacto telefono">Cel: 300 800 3482</li>
        <li class="parrafo-contacto telefono">Telefono: 373 09 40</li>
        <li class="parrafo-contacto mensaje">Email: vgarcia@proyectos.com</li>
        <li class="parrafo-contacto mensaje">Email: yaleman@imposabys.com</li>
        <br>

        <li class="parrafo-contacto"><strong> CARTAGENA </strong></li>
        <li class="parrafo-contacto aqui">Zona Franca la Candelaria</li>
        <li class="parrafo-contacto aqui">Km 9 Via Mamonal-Bodegas 37 y 38</li>
        <li class="parrafo-contacto mensaje">Email: vgarcia@imposabys.com</li>
      </div>

      <div class="contacto-col-2 parrafo-contacto">
        <div class="texto-contacto subtitulo">
          Escribenos y nos pondremos en contacto
        </div>
        <form action="" class="form-contacto">
          <div class="margin-form">

            <label for="">Nombre completo</label>
            <input class="input-form" type="text" name="" value="" placeholder="Nombre completo">
            <label for="">Empresa</label>
            <input class="input-form" type="text" name="" value="" placeholder="Empresa">
            <label for="">Mensaje</label>
            <input class="input-form-texto" type="text" name="" value="" placeholder="Mensaje">
            <div class="btn-4">Enviar</div>
          </div>

          <div class="margin-form">
            <label for="">Correo electronico</label>
            <input class="input-form" type="text" name="" value="" placeholder="nombre@correoelectronico.co">
            <label for="">Telefono</label>
            <input class="input-form" type="text" name="" value="" placeholder="Telefono">
            <div class="">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.8453363510093!2d-74.07516788528804!3d4.621668143624763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f998161facca5%3A0x8bd9c9edcbabbf8c!2sCl.+33+%2317-51%2C+Bogot%C3%A1%2C+Colombia!5e0!3m2!1ses!2ses!4v1502755566067" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </form>
      </div>

    </div>
  </section>

<?php include('call.php') ?>
<?php include('mapa.php') ?>
<?php get_footer() ?>
