<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'imposaby');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!%cu)@%/cjh59Yz|gKdus+We9N2iRw)5*!6MU=ZuP{]UdQD.1HL!)uX8hNJ5M}^w');
define('SECURE_AUTH_KEY',  'S`/PIhSv$1wW_9!s=s=Myl:RKL4dU;:K-h-Bf{:6*-caTw}SL+Di2?>pNzL*!k/|');
define('LOGGED_IN_KEY',    'EV}||b ~-?/+`hoZ>+;`l(TUXMIp<6`I%u49$Q0s6<c6ea#sitJ Bl$]z69um*dN');
define('NONCE_KEY',        'Y0BOE`#__LZ`G0aHLmUiFt#]t$YQCtq:d)$h`}2HCo*EJ4uAMPI]DAT5@AUQ-[iu');
define('AUTH_SALT',        ':!yh}z5];VO]Jeo2C/Auu9zg|q_=b8olow}}-1#tgy_RI^)Q0;~?zxqG!LtGcU[m');
define('SECURE_AUTH_SALT', '|gT CQLy:TL=4Z,e`-Kac;WHkrdi)AuaTY|:L[sg{ksXpS6AQ7v^vQ}p<sjJhjv5');
define('LOGGED_IN_SALT',   'x%)],3=MDU?o>6}(H&q.LVh~gD=W|#lD!/,z{PyH8aZ:<gQDVy %L/1^j-_B7zt%');
define('NONCE_SALT',       'o~TU)wV#Ied.+CVhjLx&/z<h;O:*XEyU<QtQxeW_vh4iq#|Tq<#(Hh3T2mDD+/c ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
